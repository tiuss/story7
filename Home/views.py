from django.shortcuts import render

mhs_name ='Titus Dirgantoro'
# Create your views here.
def index(request):
    response = {'mhs_name': mhs_name}
    return render(request,'homepage.html',response)